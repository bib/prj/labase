* 2ème test Nébuliseur Low-tech le 16 avril
** Bricolage du système
Assemblage : d'un tube de stylo bic, d'un embout dévissé d'un autre stylo, de l'embout découpé de stylo 4 couleurs, d'un impacteur bricolé avec le tube de l'encre du stylo fixé avec un élastique. Intégration d'un petit tuyau (tube encre stylo) relié à un écocup qui va servir de réservoir du liquide. Tout cela assemblé par emboîtage oua vec de la colle forte.
  - astuce : pour réduire le débit de l'eau, applatir le tuyau avec une règle.
** Protocole de test
  - Remplir l'écocup d'eau
  - Vérification de l'écoulement de l'eau (pas trop fortement)
  - Souffler dans le stylo de manière constante
  
** Résultats
  - Nébulisation reste grossière et peu régulière mais fonctionne
  - Il reste tout de même de grosses gouttes qui tombent, dû à la trop grande quantité de liquide qui ne peut être nébulisé. Trouver un système pour qu'il n'y ai pas trop d'eau disponible d'un coup à la nébulisation.
  - Problème persistant : l'eau remonte et sort par le haut du bouchon à cause de la pression
  - Est-ce que la verticalité des buses du nébuliseur n'est pas nécessaire ? La gravité empêche peut-être les gouttes de correctement s'impacter
  - Pas encore de système fermé..
  - Pression pas assez constante, essai pompe à vélo ?
  
** Objectifs
  - Trouver un moyen de limiter le débordement et les grosses gouttes qui tombent
  - Améliorer la nébulisation
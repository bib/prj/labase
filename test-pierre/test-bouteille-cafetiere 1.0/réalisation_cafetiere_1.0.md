## deuxieme serie de test le 14/04/20
Pierre Delobel

En tirant les enseignements de la veille et les discussions du groupe voila la deuxieme serie d'expériences.

### la nubulisation au stylo

Différents assemblages :
- de stylos bic et autre bouchon +stylo bic don on a rainuré le coté pour que l'eau passe et ajuster un trou de sorti suppérieur à l'arrivee d'air.
- par le petit trou du corps du stylo
- conbinaison de bouchons et stylos
...
rien ne marche pour nébuliser, les bulles des fuites font plus de gouttes et aucune eau n'estaspiré par venturi.
espaces trop gros ? volume de la chambre trop grand ?...
à travailler

### cafetiere + bouteille = boutière

Suppression du haut de la cafetiere pour déposer directement une bouteille sans fond avec le nébulisateur branché en latéral sur le haut.

#### RQ : 

- le fond est enlevé en laissant le petit rebord au tour
- un joint mousse adhésive (isolant pour fenetre) pour faire joint avec le bas de la cafetiere (pour moi, c'etait sur l'arrondi)
- je n'ai pas vraiment testé avec une toile (juste un léger voilage) c'est le système que je testais

#### résultat ; ca fonctionne à priori

le tube silicone est placé sur le trou de la cafetiere (enplacement de la soupape) et tien pas l'aspiration.
Au bruit et aux gouttelettes dans le tbe d'aspiration ça bulle bien dedans.
le joint mousse fait bien joint et j'arrive à compacter la bouteille alors que le bouchon est juste posé sur le couvercle.
le nébulisateur nébulise mais pas assez rapidement pour un test "rapide" de tete, il faut 10 à 20 minutes pour nébuliser 10ml de solution en condition de fonctionnement normales.
il faudrait avoir des produits à tester colorant avec concentration initial, volume à nébuliser et seuil de détection en sortie pour avancer sur un protocole de test.

#### compléments

La toile doit pouvoir etre placée sur la bouteille par un élastique, c'est ce qui me semble le plus simple et le moins contraignant pour la toile.
Le choix du produit définira le temps de nébulisation à faire par rapport au volume à passer et si il faut augmenter le débit de nébulisation.
un premier choix pourrait être de mettre 5 ou 10 pointes de néblisation sur notre système à imprimer.
a+

